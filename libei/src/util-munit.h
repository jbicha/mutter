/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/* This is a wrapper around munit to make it faster to use for the simple
 * type of test cases.
 *
 * Use with the MUNIT_TEST macro like this:
 *
 * MUNIT_TEST(some_test) {
 *	return MUNIT_OK;
 * }
 *
 * int main(int argc, char **argv) {
 *	return munit_tests_run(argc, argv);
 * }
 *
 */

#pragma once

#include <munit.h>

typedef MunitResult (*munit_test_func_t)(const MunitParameter params[], void *user_data);

struct test_function {
	const char *name;	/* function name */
	const char *file;	/* file name */
	munit_test_func_t func;	/* test function */
} __attribute__((aligned(16)));

/**
 * Defines a struct test_function in a custom ELF section that we can then
 * loop over in munit_tests_run() to extract the tests. This removes the
 * need of manually adding the tests to a suite or listing them somewhere.
 */
#define MUNIT_TEST(_func) \
static MunitResult _func(const MunitParameter params[], void *user_data);	\
static const struct test_function _test_##_func					\
__attribute__((used))								\
__attribute__((section("test_functions_section"))) = {				\
	.name = #_func,								\
	.func = _func,								\
	.file = __FILE__,							\
};										\
static MunitResult _func(const MunitParameter params[], void *user_data)

int
munit_tests_run(int argc, char **argv);
